#include <calc.h>

double Sum(const double& x, const double& y) {
  return x + y;
}

double Substract(const double& x, const double& y) {
  return x - y;
}

double Multiply(const double& x, const double& y) {
  return x * y;
}

double Divide(const double& x, const double& y) {
  return x / y;
}
