#include <iostream>
#include <string>

#include <calc.h>

int main(int argc, char *argv[]) {
  std::cout << "Waiting for input <+-*/ num1 num2>" << std::endl;
  char operation;
  double operand1;
  double operand2;

  std::cin >> operation;
  std::cin >> operand1;
  std::cin >> operand2;

  switch(operation) {
    case '+':
      std::cout << operand1 << " + " << operand2 << " = " << Sum(operand1, operand2) << std::endl;
      break;
    case '-':
      std::cout << operand1 << " - " << operand2 << " = " << Substract(operand1, operand2) << std::endl;
      break;
    case '*':
      std::cout << operand1 << " * " << operand2 << " = " << Multiply(operand1, operand2) << std::endl;
      break;
    case '/':
      std::cout << operand1 << " / " << operand2 << " = " << Divide(operand1, operand2) << std::endl;
      break;
    default:
      return 0;
      break; //useless
  }

  return 0;
}
